const sass = require('node-sass');

module.exports = function(grunt){
	grunt.initConfig({
		sass: {
			options: {
				implementation: sass,
				outputStyle: 'nested'
			},
			dist: {
				files: {
					'assets/css/style.css': 'assets/sass/style.scss'
				}
			}
		},
		browserify: {
			dist: {
				src: 'src/js/*.js',
				dest: 'assets/js/main.js'
			}
		},
		watch: {
			css: {
				files: '**/*.scss',
				tasks: ['sass']
			},
			bundle: {
				files: 'src/js/*.js',
				tasks: ['browserify']
			}
		}
	});
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-browserify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('default',['watch']);
}
