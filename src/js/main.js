// jQuery 3.4.1
var jQuery = $ = require('jquery');

// color-convert 2.0.1
var cc = require('color-convert');

// Function run every time input box is edited
function update(){
	// Format hexadecimal properly
	var hex = $('#hex').val();
	if (hex.length == 3){
		hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
	} else if(hex.length != 6){ return; }

	// Convert hexadecimal to HSV
	var out = cc.hex.hsv(hex);

	// Scale appropriately
	var h = Math.floor((out[0]/360)*29);
	var s = Math.floor((out[1]/100)*14);
	var v = Math.floor((out[2]/100)*14);

	// Generate background gradients
	var segs_s = 15;
	var grad_s = "linear-gradient(90deg, ";
	for (var i = 0; i < segs_s; i++){
		grad_s += "#"+ cc.hsv.hex((h/29)*360, (100/segs_s)*i, (v/15)*100) +" "+ (100/segs_s)*i +"%, ";
		grad_s += "#"+ cc.hsv.hex((h/29)*360, (100/segs_s)*i, (v/15)*100) +" "+ (100/segs_s)*(i+1) +"%";
		if (i < segs_s-1){ grad_s += ", "; }
	}
	grad_s += ")";
	$('#viv .container').css('background', grad_s);

	var segs_v = 15;
	var grad_v = "linear-gradient(90deg, ";
	for (var i = 0; i < segs_v; i++){
		grad_v += "#"+ cc.hsv.hex((h/29)*360, (s/15)*100, (100/segs_v)*i) +" "+ (100/segs_v)*i +"%, ";
		grad_v += "#"+ cc.hsv.hex((h/29)*360, (s/15)*100, (100/segs_v)*i) +" "+ (100/segs_v)*(i+1) +"%";
		if (i < segs_v-1){ grad_v += ", "; }
	}
	grad_v += ")";
	$('#brg .container').css('background', grad_v);

	// Position indicators
	var segs_h = 30;
	var adj_h = $('#hue .container').width()/segs_h/2;
	var adj_s = $('#viv .container').width()/segs_s/2;
	var adj_v = $('#brg .container').width()/segs_v/2;

	$('#hue .indicator').css('left', ($('#hue .container').width()/segs_h)*h + adj_h +"px");
	$('#viv .indicator').css('left', ($('#viv .container').width()/segs_s)*s + adj_s +"px");
	$('#brg .indicator').css('left', ($('#brg .container').width()/segs_v)*v + adj_v +"px");

	// Text indicator
	$('#hue b').text(h+1);
	$('#viv b').text(s+1);
	$('#brg b').text(v+1);

	// Color preview boxes
	$('#orig').css('background', '#'+hex);
	$('#adj').css('background', '#'+cc.hsv.hex((h/29)*360, (s/15)*100, (v/15)*100));
}

$(document).ready(function(){
	$('#hex').on('input', update);
	if ($('#hex').val().length == 0){
		$('#hex').val('000');
	}
	update();

	var segs_h = 30;
	var grad_h = "linear-gradient(90deg, ";
	for (var i = 0; i < segs_h; i++){
		grad_h += "#"+ cc.hsv.hex((360/segs_h)*i, 100, 100) +" "+ (100/segs_h)*i +"%, ";
		grad_h += "#"+ cc.hsv.hex((360/segs_h)*i, 100, 100) +" "+ (100/segs_h)*(i+1) +"%";
		if (i < segs_h-1){ grad_h += ", "; }
	}
	grad_h += ")";
	$('#hue .container').css('background', grad_h);
});
